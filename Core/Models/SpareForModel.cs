﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class SpareForModel:BaseEntity
    {
        public virtual Model Model { get; set; }
        public virtual Spare Spare { get; set; }
        public int Number { get; set; }

        public string EntityName()
        {
            return "SpareForModel";
        }
    }
}
