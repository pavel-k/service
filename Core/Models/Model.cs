﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    [DataContract]
    [KnownType(typeof(Company))]
    [KnownType(typeof(BaseEntity))]
    public class Model:BaseEntity
    {
        [DataMember]
        public virtual Company Company { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
