﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Spare:BaseEntity
    {
        public string Name { get; set; }
        public string Category { get; set; }
        
        public string EntityName()
        {
            return "Spare";
        }
    }
}
