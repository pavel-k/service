﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;


namespace Core.Models
{
    [DataContract]
    public class Company:BaseEntity
    {
        [DataMember]
        public string Name { get; set; }
        
    }
}
