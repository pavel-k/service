﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Attribute:BaseEntity
    {
        public string Name { get; set; }


        public string EntityName()
        {
            return "Attribute";
        }
    }
}
