﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    [DataContract]
    public class Car:BaseEntity
    {
        [DataMember]
        public String Number { get; set; }

        [DataMember]
        public String Owner { get; set; }

        [DataMember]
        public virtual Model Model { get; set; }
    }
}
