﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class SpareAttribute:BaseEntity
    {
        public virtual Spare Spare { get; set; }
        public virtual Attribute Attribute { get; set; }
        public String Value { get; set; }
        
        public string EntityName()
        {
            return "SpareAttribute";
        }
    }
}
