﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Attribute = Core.Models.Attribute;

namespace Service
{
    class Service
    {
        private List<Company> companies;
        private List<Model> models;
        private List<Car> cars;
        private List<SpareForModel> spareForModels;
        private List<Spare> spares;
        private List<SpareAttribute> spareAttributes;
        private List<Attribute> attributes;

        public Service()
        {
            companies = new List<Company>();
            models = new List<Model>();
            cars = new List<Car>();
            spares = new List<Spare>();
            spareForModels = new List<SpareForModel>();
            spareAttributes = new List<SpareAttribute>();
            attributes = new List<Attribute>();
        }

        public void AddCompany(Company company)
        {
            companies.Add(company);
        }

        public void AddModel(Model model)
        {
            if (companies.Contains(model.Company))
            {
                models.Add(model);
            }
    
            else throw  new Exception("Current company not exist");
        }

        //Properties
        public List<Attribute> Attributes
        {
            get { return attributes; }
        }

        public List<SpareAttribute> SpareAttributes
        {
            get { return spareAttributes; }
        }

        public List<Spare> Spares
        {
            get { return spares; }
        }

        public List<Model> Models
        {
            get { return models; }
        }

        public List<SpareForModel> SpareForModels
        {
            get { return spareForModels; }
        }

        public List<Car> Cars
        {
            get { return cars; }
        }

        public List<Company> Companies
        {
            get { return companies; }            
        }
    }
}
