﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using Data.Repository;
using Wpf.ServiceReference1;

namespace Wpf.Forms
{
    /// <summary>
    /// Interaction logic for AddModelForm.xaml
    /// </summary>
    public partial class AddModelForm : Window
    {
        private void InitCompanyListBox()
        {
            var companyRepository = new CompanyRepository();
            List<Company> companies = companyRepository.Get.ToList();

            foreach (var company in companies)
            {
                var cItem = new ComboBoxItem() { Tag = company.Id, Content = company.Name };
                CompanyListBox.Items.Add(cItem);
            }
        }

        private void SaveModel(object instance)
        {
            var model = (Model) instance;
            var modelRepository = new ModelRepository();
            modelRepository.Save(model);
        }

        public AddModelForm()
        {
            InitializeComponent();
            InitCompanyListBox();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            using (var myService = new Service1Client())
            {
                var companyRepository = new CompanyRepository();

                var id = (int) ((ComboBoxItem) CompanyListBox.SelectedValue).Tag;
                var model = new Model()
                {
                    Name = ModelName.Text,
                    Company = companyRepository.Get.FirstOrDefault(x => x.Id == id)
                };

                myService.SaveModel(model);
            }

            //SaveModel(model);
            //var th = new Thread(SaveModel);
            //th.Start(model);
            
            //Close();
            
        }
    }
}
