﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using Data;
using Data.Repository;

namespace Wpf.Forms
{
    /// <summary>
    /// Interaction logic for AddCompanyForm.xaml
    /// </summary>
    public partial class AddCompanyForm : Window
    {
        public AddCompanyForm()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();

            var cr = new CompanyRepository();
            cr.Save(new Company() { Name = companyNameBox.Text });

            /*
            using (var myService = new ServiceReference1.Service1Client())
            {
                //var cRep = new CompanyRepository();
                myService.SaveCompany(new Company() { Name = companyNameBox.Text });
            }*/

        }
    }
}
