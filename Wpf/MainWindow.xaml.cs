﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using Data.Repository;
using Wpf.Forms;
using Wpf.ServiceReference1;

namespace Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /* Repository Map:
         * 
         * 0 - null
         * 1 - Company
         * 2 - Model
         * 3 - Car 
        */

        
        readonly CompanyRepository _companyRepository = new CompanyRepository();
        readonly ModelRepository _modelRepository = new ModelRepository();
        readonly CarRepository _carRepository = new CarRepository();
  
            
        private readonly Dictionary<int, Action> _entitiesWindowMap = new Dictionary<int, Action>()
        {
            //Default
            {0, () => MessageBox.Show("Form is not implemented")},

            //Company
            {1, () => { Window x = new AddCompanyForm(); x.ShowDialog();}},

            //Model
            {2, () => { Window x = new AddModelForm(); x.ShowDialog();}}

       };

         

        private ContextMenu GetAddMenu(int id)
        {
            var addMenu = new ContextMenu();
            var addItem = new MenuItem() { Header = "Add", Tag = id };
            addItem.Click += new RoutedEventHandler(AddMenuClick); 
            addMenu.Items.Add(addItem);

            return addMenu;
        }

        private ContextMenu GetDeleteMenu(int id, int repositoryId)
        {
            var delMenu = new ContextMenu(){Tag = repositoryId};
            var delItem = new MenuItem() { Header = "Delete", Tag = id };
            delItem.Click += new RoutedEventHandler(DelMenuClick);
            delMenu.Items.Add(delItem);

            return delMenu;
        }

        private void DeleteEntity(int id, int parentId)
        {
            using (var myService = new Service1Client())
            {

                //temp hard code 
                if (parentId == 1)
                {
                    //_companyRepository.Remove(_companyRepository.Get.FirstOrDefault(x => x.Id == id));
                    myService.DeleteCompany(myService.GetCompany(id));
                }
                else if (parentId == 2)
                {
                    //_modelRepository.Remove(_modelRepository.Get.FirstOrDefault(x => x.Id == id));
                    myService.DeleteModel(myService.GetModel(id));
                }
                else if (parentId == 3)
                {
                    //_carRepository.Remove(_carRepository.Get.FirstOrDefault(x => x.Id == id));
                    myService.DeleteCar(myService.GetCar(id));
                }

                else throw new Exception("Repository not found");
            }


        }

        private void CreateTree()
        {

            Company c = _companyRepository.Get.FirstOrDefault();
            c.Name = "testtesttest";
            _companyRepository.Save(c);


            using (var myService = new Service1Client())
            {
                ServiceTree.Items.Clear();

                var root = new TreeViewItem {Header = "Service"};
                ServiceTree.Items.Add(root);

                //Company 
                var companyNode = new TreeViewItem {Header = "Company", ContextMenu = GetAddMenu(1)};
                var companies = myService.GetAllCompanies();
                foreach (var company in companies)
                {
                    var companyItem = new TreeViewItem
                    {
                        Header = company.Name,
                        ContextMenu = GetDeleteMenu(company.Id, 1)
                    };
                    companyNode.Items.Add(companyItem);
                }

                //Model
                var modelNode = new TreeViewItem {Header = "Model", ContextMenu = GetAddMenu(2)};
                var models = myService.GetAllModels();
                foreach (var model in models)
                {
                    var modelItem = new TreeViewItem {Header = model.Name, ContextMenu = GetDeleteMenu(model.Id, 2)};
                    modelNode.Items.Add(modelItem);
                }
                
                //Car
                var carNode = new TreeViewItem {Header = "Car", ContextMenu = GetAddMenu(0)};
                var cars = myService.GetAllCars();
                foreach (var car in cars)
                {
                    var carItem = new TreeViewItem {Header = car.Number, ContextMenu = GetDeleteMenu(car.Id, 3)};
                    carNode.Items.Add(carItem);
                }

                //Not impl forms 
                var spareNode = new TreeViewItem {Header = "Spare", ContextMenu = GetAddMenu(0)};
                

                

                root.Items.Add(companyNode);
                root.Items.Add(modelNode);
                root.Items.Add(carNode);
                root.Items.Add(spareNode);

            }

        }

        public MainWindow()
        {
            InitializeComponent();
            CreateTree();    
        }

        private void AddMenuClick(object sender, RoutedEventArgs e)
        {
            var id = (int)((MenuItem) sender).Tag;
            _entitiesWindowMap[id].Invoke();
            CreateTree();
        }

        private void DelMenuClick(object sender, RoutedEventArgs e)
        {
            var item = (MenuItem) sender;
            var itemId = (int) item.Tag;
            var repositoryId = (int)((ContextMenu)item.Parent).Tag;

            DeleteEntity(itemId, repositoryId);
            CreateTree();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CreateTree();
        }

        
    }
}
