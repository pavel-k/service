﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    class ServiceContextSingleton
    {
        private static ServiceContext _serviceContext = new ServiceContext();

        public static ServiceContext ServiceContext
        {
            get
            {
                if (_serviceContext != null)
                {
                    _serviceContext = new ServiceContext();
                    //Database.SetInitializer(new DropCreateDatabaseAlways<ServiceContext>());
                    //Database.SetInitializer<ServiceContext>(null);
                }

                return _serviceContext;
            }
        }

        private ServiceContextSingleton() {}
    }
}
