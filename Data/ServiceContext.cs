﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using Attribute = Core.Models.Attribute;

namespace Data
{
    public class ServiceContext:DbContext
    {
        public DbSet<Company> Companies { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<SpareForModel> SpareForModels { get; set; }
        public DbSet<Spare> Spares { get; set; }
        public DbSet<Attribute> Attributes { get; set; }
        public DbSet<SpareAttribute> SpareAttributes { get; set; }

        public ServiceContext()
        {
            this.Configuration.ProxyCreationEnabled = true;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Model>().HasOptional(a => a.Company).WithOptionalDependent().WillCascadeOnDelete(true);
            modelBuilder.Entity<Car>().HasOptional(a => a.Model).WithOptionalDependent().WillCascadeOnDelete(true);
        }
    }


}
