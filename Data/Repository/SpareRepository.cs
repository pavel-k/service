﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;

namespace Data.Repository
{
    public class SpareRepository:IRepository<Spare>
    {
        private readonly ServiceContext _db = ServiceContextSingleton.ServiceContext;

        public IQueryable<Spare> Get
        {
            get
            {
                return _db.Spares;
            }
        }

        public void Save(Spare instance)
        {
            _db.Spares.Add(instance);
            _db.SaveChanges();
        }

        public void Remove(Spare instance)
        {
            _db.Spares.Remove(instance);
            _db.SaveChanges();
        }
    }
}
