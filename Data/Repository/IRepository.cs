﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;

namespace Data.Repository
{
    public interface IRepository<T> //where T : BaseEntity
    {
        IQueryable<T> Get { get; }
        void Save(T instance);
        void Remove(T instance);
    }
}
