﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;

namespace Data.Repository
{
    public class SpareAttribyteRepository:IRepository<SpareAttribute>
    {
        private readonly ServiceContext _db = ServiceContextSingleton.ServiceContext;

        public IQueryable<SpareAttribute> Get
        {
            get
            {
                return _db.SpareAttributes;
            }
        }

        public void Save(SpareAttribute instance)
        {
            _db.SpareAttributes.Add(instance);
            _db.SaveChanges();
        }

        public void Remove(SpareAttribute instance)
        {
            _db.SpareAttributes.Remove(instance);
            _db.SaveChanges();
        }
    }
}
