﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Attribute = Core.Models.Attribute;

namespace Data.Repository
{
    public class AttribyteRepository:IRepository<Attribute>
    {
        private readonly ServiceContext _db = ServiceContextSingleton.ServiceContext;

        public IQueryable<Attribute> Get
        {
            get
            {
                return _db.Attributes;
            }
        }
        
        public void Save(Attribute instance)
        {
            _db.Attributes.Add(instance);
            _db.SaveChanges();
        }

        public void Remove(Attribute instance)
        {
            _db.Attributes.Remove(instance);
            _db.SaveChanges();
        }
    }
}
