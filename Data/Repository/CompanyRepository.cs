﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;

namespace Data.Repository
{
    public class CompanyRepository:IRepository<Company>
    {
        private readonly ServiceContext _db = ServiceContextSingleton.ServiceContext;

        public IQueryable<Company> Get
        {
            get
            {
                return _db.Companies;
            }
        }
        
        public void Save(Company instance)
        {
            _db.Companies.Add(instance);
            _db.SaveChanges();
        }

        public void Remove(Company instance)
        {
            _db.Companies.Attach(instance);
            _db.Companies.Remove(instance);
            
            
            _db.SaveChanges();
        }
    }
}
