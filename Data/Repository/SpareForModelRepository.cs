﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;

namespace Data.Repository
{
    public class SpareForModelRepository:IRepository<SpareForModel>
    {
        private readonly ServiceContext _db = ServiceContextSingleton.ServiceContext;

        public IQueryable<SpareForModel> Get
        {
            get
            {
                return _db.SpareForModels;
            }
        }

        public void Save(SpareForModel instance)
        {
            _db.SpareForModels.Add(instance);
            _db.SaveChanges();
        }

        public void Remove(SpareForModel instance)
        {
            _db.SpareForModels.Remove(instance);
            _db.SaveChanges();
        }
    }
}
