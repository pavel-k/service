﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;

namespace Data.Repository
{
    public class CarRepository:IRepository<Car>
    {
        private readonly ServiceContext _db = ServiceContextSingleton.ServiceContext;

        public IQueryable<Car> Get
        {
            get
            {
                return _db.Cars;
            }
        }
        
        public void Save(Car instance)
        {
            _db.Cars.Add(instance);
            _db.SaveChanges();
        }

        public void Remove(Car instance)
        {
            _db.Cars.Remove(instance);
            _db.SaveChanges();
        }
    }
}
