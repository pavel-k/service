﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;

namespace Data.Repository
{
    public class ModelRepository:IRepository<Model>
    {
        private readonly ServiceContext _db = ServiceContextSingleton.ServiceContext;

        public IQueryable<Model> Get
        {
            get
            {
                return _db.Models;
            }
        }
        
        public void Save(Model instance)
        {
            _db.Models.Add(instance);
            _db.SaveChanges();
        }

        public void Remove(Model instance)
        {
            _db.Models.Remove(instance);
            _db.SaveChanges();
        }
    }
}
